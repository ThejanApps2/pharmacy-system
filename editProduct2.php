<?php

include("db.php");
mysqli_select_db($con, "pharmacy") or die("Can't select DB");


    $pId =$_POST['id'];
    
    $r="SELECT * FROM product WHERE pId=$pId";

    $y = mysqli_query($con,$r);

    $result = $con->query("SELECT pName,price,qty,des FROM product WHERE pId=$pId");
  
    $row = $result->fetch_assoc();



    $pName = $row['pName'];
    $price = $row['price'];
    $qty = $row['qty'];
    $des = $row['des'];

   
?>

<?php require 'header.php'; ?>

<div class="container">
  <div class="card mt-5">
    <div class="card-header">
      <h2>Product</h2>
      <h6>Edit Product Details</h6>
    </div>
    <div class="card-body">
      <?php if(!empty($message)): ?>
        <div class="alert alert-success">
          <?= $message; ?>
        </div>
      <?php endif; ?>

      <form name="addProduct" action="editProductP.php" method="post">

        <div class="form-group">
          <label for="username">Product Name : </label>
          <input type="text" name="pName" placeholder="Product Name.."    value=<?php echo $pName; ?>  class="form-control" required='required' ><br>
        </div>

        <div class="form-group">
          <label for="username">Price : </label>
          <input type="text" name="price" placeholder="Price.."  value=<?php echo $price; ?> class="form-control" required='required'><br>
        </div>

        <div class="form-group">
          <label for="username">Quantity : </label>
          <input type="text" name="qty" placeholder="Quantity.."  value=<?php echo $qty; ?> class="form-control" required='required'><br>
        </div>

        <div class="form-group">
          <label for="username">Description : </label>
          <input type="text" name="des" placeholder="Description.."   value=<?php echo $des; ?> class="form-control" required='required'><br>
        </div>

        <div class="form-group">
          <input type="submit" value = "Edit Product"  class="btn btn-info" name= "edit">
          
      </form>
    </div>
  </div>
</div> 



<?php require 'footer.php'; ?>