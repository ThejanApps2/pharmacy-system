
<?php require 'header.php'; ?>

<div class="container">
  <div class="card mt-5">
    <div class="card-header">
      <h2>Supplier</h2>
      <h6>Add Supplier Details</h6>
    </div>
    <div class="card-body">
      <?php if(!empty($message)): ?>
        <div class="alert alert-success">
          <?= $message; ?>
        </div>
      <?php endif; ?>
      <form name="addSupplier" action="addSupplierS.php" method="post">

        <div class="form-group">
          <label for="username">SupplierName : </label>
          <input type="text" name="SupplierName" placeholder="SupplierName.."  class="form-control" required='required'><br>
        </div>

        <div class="form-group">
          <label for="username">Email : </label>
          <input type="text" name="Email" placeholder="E-mail.."  class="form-control" required='required'><br>
        </div>

        <div class="form-group">
          <label for="username">Phone : </label>
          <input type="text" name="Phone" placeholder="Phone.."  class="form-control" required='required'><br>
        </div>

        <div class="form-group">
          <label for="username">Address : </label>
          <input type="text" name="Address" placeholder="Address.."  class="form-control" required='required'><br>
        </div>

        <div class="form-group">
          <input type="submit" value = "Add Supplier"  class="btn btn-info" name= "add">
          
      </form>

      


    </div>

    </div>
  </div>
</div>

<?php require 'footer.php'; ?>