<?php require 'header.php'; ?>

<div class="container">
  <div class="card mt-5">
    <div class="card-header">
      <h2>Customer</h2>
      <h6>Delete Customer Details</h6>
</div>
  <div class="card-body">
    <?php if(!empty($message)): ?>
      <div class="alert alert-success">
          <?= $message; ?>
      </div>

<?php endif; ?>

<?php 

echo '<form name="deleteCustomer" action="delete_CustomerP.php" method="post">';
echo '<div class="form-group">';
echo '<label for="username">Customer Number : </label>';
         
include("db.php");
mysqli_select_db($con, "pharmacy") or die("Can't select DB");

$r = "SELECT cId FROM customer";
$y = mysqli_query($con,$r);

echo '<select name="id" class="form-control">';

$result = $con->query("SELECT cId FROM customer");
  
while ($row = $result->fetch_assoc()) 
{
    unset($id);
    $id = $row['cId'];
    echo '<option value="'.$id.'">'.$id.'</option>';
}

echo '</select>';
echo '</div>';
echo '<div class="form-group">';
echo '<input type="submit" value = "Delete Customer"  class="btn btn-info" name= "delete">';
echo '</form>';

?>


</div>
</div>
</div>

<?php require 'footer.php'; ?>

