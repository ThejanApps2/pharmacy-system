<?php require 'header.php'; ?>

<div class="container">
  <div class="card mt-5">
    <div class="card-header">
      <h2>Supplier</h2>
      <h6>Delete Supplier Details</h6>
</div>
  <div class="card-body">
    <?php if(!empty($message)): ?>
      <div class="alert alert-success">
          <?= $message; ?>
      </div>

<?php endif; ?>

<?php 

echo '<form name="deleteSupplier" action="deleteSupplierS.php" method="post">';
echo '<div class="form-group">';
echo '<label for="username">Supplier Number : </label>';
         
include("db.php");
mysqli_select_db($con, "pharmacy") or die("Can't select DB");

$r = "SELECT sId FROM supplier";
$y = mysqli_query($con,$r);

echo '<select name="id" class="form-control">';

$result = $con->query("SELECT sId FROM supplier");
  
while ($row = $result->fetch_assoc()) 
{
    unset($id);
    $id = $row['sId'];
    echo '<option value="'.$id.'">'.$id.'</option>';
}

echo '</select>';
echo '</div>';
echo '<div class="form-group">';
echo '<input type="submit" value = "Delete Supplier"  class="btn btn-info" name= "delete">';
echo '</form>';

?>


</div>
</div>
</div>

<?php require 'footer.php'; ?>

