
<?php require 'header.php'; ?>

<div class="container">
  <div class="card mt-5">
    <div class="card-header">
      <h2>Pharmacy Management System</h2>
    </div>
    <div class="card-body">
      <?php if(!empty($message)): ?>
        <div class="alert alert-success">
          <?= $message; ?>
        </div>
      <?php endif; ?>
      <!-- <form name="login" action="login.php" method="post">
        <div class="form-group">
          <label for="username">User Name : </label>
          <input type="text" name="name" placeholder="Your User Name.."  class="form-control" required='required'><br>
        </div>
        <div class="form-group">
          <label for="password">Password : </label>
          <input type="password" name="pword" placeholder="Your password.."  class="form-control" required='required'><br>
                
        
         
        </div>
        <div class="form-group">
          <input type="submit" value = "Log in"  class="btn btn-info" name= "login">
           -->

      <div class="form-group">
      <ul >
      					<li>Product</li>
                        <div class="form-group">
                        	<button class="btn btn-info" onclick="myFunction1()">Add Details</button>
                        	<button class="btn btn-info" onclick="myFunction2()">Delete Details</button>
                        	<button class="btn btn-info" onclick="myFunction3()">Edit Details</button>
          				</div>

                        <li>Employee</li>
                        <div class="form-group">
                        	<button class="btn btn-info" onclick="addEmployee()">Add Details</button>
                        	<button class="btn btn-info" onclick="deleteEmployee()">Delete Details</button>
                        	<button class="btn btn-info" onclick="editEmployee()">Edit Details</button>
          				</div>

          				<li>Customer</li>
                        <div class="form-group">
                        	<button class="btn btn-info" onclick="Cus_add()">Add Customer</button>
                        	<button class="btn btn-info" onclick="Cus_delete()">Delete Customer</button>
                        	<button class="btn btn-info" onclick="Cus_edit()">Edit Customer</button>
          				</div>
                  <li>Supplier</li>
                        <div class="form-group">
                          <button class="btn btn-info" onclick="addSupplier()">Add Details</button>
                          <button class="btn btn-info" onclick="deleteSupplier()">Delete Details</button>
                          <button class="btn btn-info" onclick="editSupplier()">Edit Details</button>
                  </div>                                
       </ul>
       </div>


       

<script>
function myFunction() {
  location.replace("index.php")
}
</script>

<script>
function myFunction1() {
  location.replace("addProduct.php")
}
</script>

<script>
function myFunction2() {
  location.replace("deleteProduct.php")
}
</script>

<script>
function myFunction3() {
  location.replace("editProduct1.php")
}
</script>

<script>
function addEmployee() {
  location.replace("addEmployee.php")
}
</script>

<script>
function Cus_view() {
  location.replace("editProduct1.php")
}
</script>

<script>
function Cus_add() {
  location.replace("add_Customer.php")
}
</script>

<script>
function deleteEmployee() {
  location.replace("deleteEmployee.php")
}
</script>

<script>
function Cus_delete() {
  location.replace("delete_Customer.php")
}
</script>

<script>
function editEmployee() {
  location.replace("editEmployee.php")
}
</script>

<script>
function Cus_edit() {
  location.replace("edit_Customer1.php")
}
</script>
<script>
function addSupplier() {
  location.replace("addSupplier.php")
}
</script>

<script>
function deleteSupplier() {
  location.replace("deleteSupplier.php")
}
</script>

<script>
function editSupplier() {
  location.replace("editSupplier1.php")
}
</script>


    </div>
  </div>
</div>

<?php require 'footer.php'; ?>