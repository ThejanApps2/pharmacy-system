
<?php require 'header.php'; ?>

<div class="container">
  <div class="card mt-5">
    <div class="card-header">
      <h2>Employee</h2>
      <h6>Add Employee Details</h6>
    </div>
    <div class="card-body">
      <?php if(!empty($message)): ?>
        <div class="alert alert-success">
          <?= $message; ?>
        </div>
      <?php endif; ?>
      <form name="addEmployee" action="addEmployeeP.php" method="post">

        <div class="form-group">
          <label for="name">Employee Name : </label>
          <input type="text" name="eName" placeholder="Employee Name.."  class="form-control" required='required'><br>
        </div>

        <div class="form-group">
          <label for="email">Email : </label>
          <input type="email" name="email" placeholder="Email.."  class="form-control" required='required'><br>
        </div>

        <div class="form-group">
          <label for="password">Password : </label>
          <input type="password" name="password" placeholder="Password.."  class="form-control" required='required'><br>
        </div>

        <div class="form-group">
          <label for="nic">NIC : </label>
          <input type="text" name="nic" placeholder="NIC.."  class="form-control" required='required'><br>
        </div>

        <div class="form-group">
          <input type="submit" value = "Add Employee"  class="btn btn-info" name= "add">
          
      </form>

      


    </div>

    </div>
  </div>
</div>

<?php require 'footer.php'; ?>