<?php require 'header.php'; ?>

<div class="container">
  <div class="card mt-5">
    <div class="card-header">
      <h2>Employee</h2>
      <h6>Edit Employee Details</h6>
</div>
  <div class="card-body">
    <?php if(!empty($message)): ?>
      <div class="alert alert-success">
          <?= $message; ?>
      </div>

<?php endif; ?>

<?php 

echo '<form name="editEmployee" action="editEmployee2.php" method="post">';
echo '<div class="form-group">';
echo '<label for="username">Employee Email : </label>';
         
include("db.php");
mysqli_select_db($con, "pharmacy") or die("Can't select DB");

$r = "SELECT email FROM employee";
$y = mysqli_query($con,$r);

echo '<select name="email" class="form-control">';

$result = $con->query("SELECT email FROM employee");
  
while ($row = $result->fetch_assoc()) 
{
    unset($email);
    $email = $row['email'];
    echo '<option value="'.$email.'">'.$email.'</option>';
}

echo '</select>';
echo '</div>';
echo '<div class="form-group">';
echo '<input type="submit" value = "Edit Employee"  class="btn btn-info" name= "edit">';
echo '</form>';

?>

<div class="form-group"></div>
      <div class="form-group" align = "right">
      <button  onclick="myFunction()" >Go to Dashboard</button>
      <script>
      function myFunction() 
      {
        location.replace("dashboard.php")
      }
      </script>

</div>
</div>
</div>

<?php require 'footer.php'; ?>

