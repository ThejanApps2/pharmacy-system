create database db_pharmacy;
use db_pharmacy;

create table employee (
  id  int(11) auto_increment primary key,
  name varchar(30) not null,
  email varchar(30) not null,
  password varchar(30),
  salary varchar(10),
  phone varchar(12)
);

create table product (
  id  int(11) auto_increment primary key,
  name varchar(30) not null,
  price NUMERIC not null,
  quantity INTEGER(30)
);

create table customer (
  id  int(11) auto_increment primary key,
  name varchar(30) not null,
  email varchar(30) not null,
  phone varchar(12),
  address varchar(50)
);

create table supplier (
  id  int(11) auto_increment primary key,
  name varchar(30) not null,
  email varchar(30) not null,
  phone varchar(12),
  address varchar(50)
);