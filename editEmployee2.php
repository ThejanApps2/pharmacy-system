<?php

include("db.php");
mysqli_select_db($con, "pharmacy") or die("Can't select DB");

    $email =$_POST['email'];
    
    $r="SELECT * FROM employee WHERE email=$email";

    $y = mysqli_query($con,$r);

    $result = $con->query("SELECT name,email,password,nic FROM employee WHERE email='".$email."'");
  
    $row = $result->fetch_assoc();



    $name = $row['name'];
    $email = $row['email'];
    $password = $row['password'];
    $nic = $row['nic'];

   
?>

<?php require 'header.php'; ?>

<div class="container">
  <div class="card mt-5">
    <div class="card-header">
      <h2>Employee</h2>
      <h6>Edit Employee Details</h6>
    </div>
    <div class="card-body">
      <?php if(!empty($message)): ?>
        <div class="alert alert-success">
          <?= $message; ?>
        </div>
      <?php endif; ?>

      <form name="editEmployee" action="editEmployeeP.php" method="post">

        <div class="form-group">
          <label for="username">Employee Name : </label>
          <input type="text" name="name" placeholder="Employee Name.."    value=<?php echo $name; ?>  class="form-control" required='required' ><br>
        </div>

        <div class="form-group">
          <label for="username">Email : </label>
          <input type="email" name="email" placeholder="Email.."  value=<?php echo $email; ?> class="form-control" required='required'><br>
        </div>

        <div class="form-group">
          <label for="username">Password : </label>
          <input type="password" name="password" placeholder="Password.."  value=<?php echo $password; ?> class="form-control" required='required'><br>
        </div>

        <div class="form-group">
          <label for="username">NIC : </label>
          <input type="text" name="nic" placeholder="NIC.."   value=<?php echo $nic; ?> class="form-control" required='required'><br>
        </div>

        <div class="form-group">
          <input type="submit" value = "Edit Employee"  class="btn btn-info" name= "edit">
          
      </form>
    </div>
  </div>
</div> 



<?php require 'footer.php'; ?>