
<?php require 'header.php'; ?>

<div class="container">
  <div class="card mt-5">
    <div class="card-header">
      <h2>Customer</h2>
      <h6>Add Customer Details</h6>
    </div>
    <div class="card-body">
      <?php if(!empty($message)): ?>
        <div class="alert alert-success">
          <?= $message; ?>
        </div>
      <?php endif; ?>
      <form name="addCustomer" action="add_CustomerP.php" method="post">

        <div class="form-group">
          <label for="username">Customer Name : </label>
          <input type="text" name="name" placeholder="Customer Name.."  class="form-control" required='required'><br>
        </div>

        <div class="form-group">
          <label for="username">Email : </label>
          <input type="text" name="email" placeholder="Email.."  class="form-control" required='required'><br>
        </div>

        <div class="form-group">
          <label for="username">Phone : </label>
          <input type="text" name="phone" placeholder="Phone.."  class="form-control" required='required'><br>
        </div>

        <div class="form-group">
          <label for="username">NIC : </label>
          <input type="text" name="NIC" placeholder="NIC.."  class="form-control" required='required'><br>
        </div>

        <div class="form-group">
          <input type="submit" value = "Add Customer"  class="btn btn-info" name= "add">
          
      </form>

      


    </div>

    </div>
  </div>
</div>

<?php require 'footer.php'; ?>