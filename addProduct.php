
<?php require 'header.php'; ?>

<div class="container">
  <div class="card mt-5">
    <div class="card-header">
      <h2>Product</h2>
      <h6>Add Product Details</h6>
    </div>
    <div class="card-body">
      <?php if(!empty($message)): ?>
        <div class="alert alert-success">
          <?= $message; ?>
        </div>
      <?php endif; ?>
      <form name="addProduct" action="addProductP.php" method="post">

        <div class="form-group">
          <label for="username">Product Name : </label>
          <input type="text" name="pName" placeholder="Product Name.."  class="form-control" required='required'><br>
        </div>

        <div class="form-group">
          <label for="username">Price : </label>
          <input type="text" name="price" placeholder="Price.."  class="form-control" required='required'><br>
        </div>

        <div class="form-group">
          <label for="username">Quantity : </label>
          <input type="text" name="qty" placeholder="Quantity.."  class="form-control" required='required'><br>
        </div>

        <div class="form-group">
          <label for="username">Description : </label>
          <input type="text" name="desc" placeholder="Description.."  class="form-control" required='required'><br>
        </div>

        <div class="form-group">
          <input type="submit" value = "Add Product"  class="btn btn-info" name= "add">
          
      </form>

      


    </div>

    </div>
  </div>
</div>

<?php require 'footer.php'; ?>