<?php require 'header.php'; ?>

<div class="container">
  <div class="card mt-5">
    <div class="card-header">
      <h2>Product</h2>
      <h6>Delete Product Details</h6>
</div>
  <div class="card-body">
    <?php if(!empty($message)): ?>
      <div class="alert alert-success">
          <?= $message; ?>
      </div>

<?php endif; ?>

<?php 

echo '<form name="deleteProduct" action="deleteProductP.php" method="post">';
echo '<div class="form-group">';
echo '<label for="username">Product Number : </label>';
         
include("db.php");
mysqli_select_db($con, "pharmacy") or die("Can't select DB");

$r = "SELECT pId FROM product";
$y = mysqli_query($con,$r);

echo '<select name="id" class="form-control">';

$result = $con->query("SELECT pId FROM product");
  
while ($row = $result->fetch_assoc()) 
{
    unset($id);
    $id = $row['pId'];
    echo '<option value="'.$id.'">'.$id.'</option>';
}

echo '</select>';
echo '</div>';
echo '<div class="form-group">';
echo '<input type="submit" value = "Delete Product"  class="btn btn-info" name= "delete">';
echo '</form>';

?>


</div>
</div>
</div>

<?php require 'footer.php'; ?>

