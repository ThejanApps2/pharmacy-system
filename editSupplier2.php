<?php

include("db.php");
mysqli_select_db($con, "pharmacy") or die("Can't select DB");


    $sId =$_POST['id'];
    
    $r="SELECT * FROM supplier WHERE sId=$sId";

    $y = mysqli_query($con,$r);

    $result = $con->query("SELECT sName,email,phone,address FROM supplier WHERE sId=$sId");
  
    $row = $result->fetch_assoc();



    $sName = $row['sName'];
    $email = $row['email'];
    $phone = $row['phone'];
    $address = $row['address'];

   
?>

<?php require 'header.php'; ?>

<div class="container">
  <div class="card mt-5">
    <div class="card-header">
      <h2>Supplier</h2>
      <h6>Edit Supplier Details</h6>
    </div>
    <div class="card-body">
      <?php if(!empty($message)): ?>
        <div class="alert alert-success">
          <?= $message; ?>
        </div>
      <?php endif; ?>

      <form name="addSupplier" action="editSupplierS.php" method="post">

        <div class="form-group">
          <label for="username">SupplierName : </label>
          <input type="text" name="SupplierName" placeholder="SupplierName.."    value=<?php echo $sName; ?>  class="form-control" required='required' ><br>
        </div>

        <div class="form-group">
          <label for="username">Email : </label>
          <input type="text" name="Email" placeholder="E-mail.."  value=<?php echo $email; ?> class="form-control" required='required'><br>
        </div>

        <div class="form-group">
          <label for="username">Phone : </label>
          <input type="text" name="Phone" placeholder="Phone.."  value=<?php echo $phone; ?> class="form-control" required='required'><br>
        </div>

        <div class="form-group">
          <label for="username">Address : </label>
          <input type="text" name="Address" placeholder="Address.."   value=<?php echo $address; ?> class="form-control" required='required'><br>
        </div>

        <div class="form-group">
          <input type="submit" value = "Edit Supplier"  class="btn btn-info" name= "edit">
          
      </form>
    </div>
  </div>
</div> 



<?php require 'footer.php'; ?>