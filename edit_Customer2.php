<?php

include("db.php");
mysqli_select_db($con, "pharmacy") or die("Can't select DB");


    $cId =$_POST['id'];
    
    $r="SELECT * FROM customer WHERE cId=$cId";

    $y = mysqli_query($con,$r);

    $result = $con->query("SELECT name,email,phone,NIC FROM customer WHERE cId=$cId");
  
    $row = $result->fetch_assoc();

    $name = $row['name'];
    $email = $row['email'];
    $phone = $row['phone'];
    $NIC = $row['NIC'];

   
?>

<?php require 'header.php'; ?>

<div class="container">
  <div class="card mt-5">
    <div class="card-header">
      <h2>Customer</h2>
      <h6>Edit Customer Details</h6>
    </div>
    <div class="card-body">
      <?php if(!empty($message)): ?>
        <div class="alert alert-success">
          <?= $message; ?>
        </div>
      <?php endif; ?>

      <form name="editCustomer" action="edit_CustomerP.php" method="post">

        <div class="form-group">
          <label for="username">Customer Name : </label>
          <input type="text" name="cId" placeholder="Customer ID."    value=<?php echo $cId; ?>  class="form-control" required='required' readonly><br>
        </div>

        <div class="form-group">
          <label for="username">Customer Name : </label>
          <input type="text" name="name" placeholder="Customer Name."    value=<?php echo $name; ?>  class="form-control" required='required' ><br>
        </div>

        <div class="form-group">
          <label for="username">Email : </label>
          <input type="text" name="email" placeholder="Email."  value=<?php echo $email; ?> class="form-control" required='required'><br>
        </div>

        <div class="form-group">
          <label for="username">Phone : </label>
          <input type="text" name="phone" placeholder="Phone."  value=<?php echo $phone; ?> class="form-control" required='required'><br>
        </div>

        <div class="form-group">
          <label for="username">NIC : </label>
          <input type="text" name="NIC" placeholder="NIC."   value=<?php echo $NIC; ?> class="form-control" required='required'><br>
        </div>

        <div class="form-group">
          <input type="submit" value = "Edit Customer"  class="btn btn-info" name= "edit">
          
      </form>
    </div>
  </div>
</div> 



<?php require 'footer.php'; ?>